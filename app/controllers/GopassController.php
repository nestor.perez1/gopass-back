<?php

class GopassController extends ControllerBase {

    public function registrarAction() {

        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
            "user_name",
            "user_email",
            "user_ciudad",
            "user_clave"
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

            	try {
                    $client = new Users();
                    $client->email = $dataRequest->user_email;
                    $client->name = $dataRequest->user_name;
                    $client->password = $dataRequest->user_clave;
                    $client->id_ciudad_user = $dataRequest->user_ciudad;
                            
                           
                    if($client->save()){

                            $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                                "return" => true,
                                "data" => $client,
                                "status" => ControllerBase::SUCCESS
                            ));
                    }else{
                        $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                            "return" => false,
                            "message" => "Registro no guardado",
                            "status" => ControllerBase::FAILED
                        ));
                    }
                        
                }catch (Exception $e) {
//echo print_r($e);die;
                  
                }	
            
        }
    }

    public function loginAction() {

        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
            "user_email",
            "user_clave"
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

                try {
                    $users = Users::findFirst(array(
                    "conditions" => "email = ?1  and password = ?2",
                    "bind" => array(1 => $dataRequest->user_email, 2 => $dataRequest->user_clave)
                ));

                    if($users){
                            $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                                "return" => true,
                                "data" => $users,
                                "status" => ControllerBase::SUCCESS
                            ));
                    }else{
                        $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                            "return" => false,
                            "message" => "Registro no guardado",
                            "status" => ControllerBase::FAILED
                        ));
                    }
                        
                }catch (Exception $e) {
//echo print_r($e);die;
                  
                }   
            
        }
    }

    public function ListadoPaisesAction() {
        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
           
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

                try {


                    $lol = new Users();
                    $resp = $lol->listPais();
                    $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                        "return" => true,
                        "data" => $resp->fetchAll(PDO::FETCH_ASSOC),
                        "status" => ControllerBase::SUCCESS
                    ));
                
                    
                        
                }catch (Exception $e) {
                     $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                        "return" => false,
                        "message" => "Sin datos guardado",
                        "status" => ControllerBase::FAILED
                    ));
//echo print_r($e);die;
                   //$this->logError($e, $dataRequest);
                }   
            
        }

    }

    public function ListadoDetaramentosAction() {
        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
           "id"
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

                try {


                    $lol = new Users();
                    $resp = $lol->listDetartamento($dataRequest->id);
                    $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                        "return" => true,
                        "data" => $resp->fetchAll(PDO::FETCH_ASSOC),
                        "status" => ControllerBase::SUCCESS
                    ));
                
                    
                        
                }catch (Exception $e) {
                     $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                        "return" => false,
                        "message" => "Sin datos guardado",
                        "status" => ControllerBase::FAILED
                    ));
//echo print_r($e);die;
                   //$this->logError($e, $dataRequest);
                }   
            
        }

    }

    public function ListadoCiudadesAction() {
        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
           "id"
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

                try {


                    $lol = new Users();
                    $resp = $lol->listCiudades($dataRequest->id);
                    $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                        "return" => true,
                        "data" => $resp->fetchAll(PDO::FETCH_ASSOC),
                        "status" => ControllerBase::SUCCESS
                    ));
                
                    
                        
                }catch (Exception $e) {
                     $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                        "return" => false,
                        "message" => "Sin datos guardado",
                        "status" => ControllerBase::FAILED
                    ));
//echo print_r($e);die;
                   //$this->logError($e, $dataRequest);
                }   
            
        }

    }

    public function EnviarEmailAction() {
        $dataRequest = $this->request->getJsonPost();
        $dateTime = new \DateTime();
        $fields = array(
           "email",
           "nombre",
           "asunto",
           "mesaje"
        );

        $optional = array(
            
        );

        if ($this->_checkFields($dataRequest, $fields, $optional)) {

                try {

                    if($this->Send_Email($dataRequest->email,$dataRequest->nombre,$dataRequest->mesaje,$dataRequest->asunto)){
                        
                        
                        $this->setJsonResponse(ControllerBase::SUCCESS, ControllerBase::SUCCESS_MESSAGE, array(
                            "return" => true,
                            "data" => "Mensaje enviado",
                            "status" => ControllerBase::SUCCESS
                        ));
                
                    }
                        
                }catch (Exception $e) {
                     $this->setJsonResponse(ControllerBase::FAILED, " Failed register client", array(
                        "return" => false,
                        "message" => "Sin datos guardado",
                        "status" => ControllerBase::FAILED
                    ));
//echo print_r($e);die;
                   //$this->logError($e, $dataRequest);
                }   
            
        }
    }



}   