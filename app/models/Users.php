<?php

use \Phalcon\Mvc\Model;

class Users extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;
    /**
     *
     * @var string
     */
    public $contac_name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $document;

    /**
     *
     * @var integer
     */
    public $password;

    /**
     *
     * @var integer
     */
    public $created_at;

    /**
     *
     * @var integer
     */
    public $fec_nac;

    /*
     * @var integer
     */
    public $phone;

    /**
     *
     * @var integer
     */
    public $document_type; // 1 CC - 2 CE - 3 Pasaporte - 4 NIT - 5 Otro

    /**
     *
     * @var integer
     */
    public $user_type;

/**
     *
     * @var integer
     */
    public $client_type;  // 0 Natural - Mayor a 0 Comercio del tipo en tabla Client_Type

    /**
     *
     * @var string
     */
    public $client_image;
    
    /**
     *
     * @var integer
     */
    public $id_genero; // 1 Masculino - 2 Femenino

    /**
     *
     * @var string
     */
    public $lat_act;

    /**
     *
     * @var string
     */
    public $log_act;

    /**
     *
     * @var string
     */
    public $locked;

     /**
     *
     * @var string
     */
    public $conect;

    /**
     *
     * @var string
     */
    public $updated_at;

  /**
     *
     * @var string
     */
    public $wallet;

  /**
     *
     * @var string
     */
    public $wallet_limite;

    /**
     *
     * @var string
     */
    public $user_nick;


    /**
     *
     * @var string
     */
    public $client_red_social_id;
    
    /**
     *
     * @var string
     */
    public $id_ciudad_user;
    
    /**
     *
     * @var string
     */
    public $id_preferencias;

    /**
     *
     * @var string
     */
    public $activo;
    

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    

    public function listPais(){
        $sql = "SELECT DISTINCT pais, id_pais  
                FROM geo_pais ORDER BY id_pais LIMIT 1121";

        $prepare = $this->getDi()->getShared("db")->prepare($sql);
        $prepare->execute();
 
        return $prepare;
    }

    public function listCiudades($id_Detartamento){
        $sql = "SELECT DISTINCT ciudad, id_ciudad
                FROM geo_ciudad WHERE id_departamento = $id_Detartamento AND estado = 1 ORDER BY id_ciudad LIMIT 1121";
        $prepare = $this->getDi()->getShared("db")->prepare($sql);
        $prepare->execute();
        return $prepare;
    }

    public function listDetartamento($id_Pais){
        $sql = "SELECT DISTINCT departamento, id_departamento  
                FROM geo_departamento WHERE id_pais = $id_Pais AND estado = 1 ORDER BY id_departamento LIMIT 1121";
//echo $sql;die;
        $prepare = $this->getDi()->getShared("db")->prepare($sql);
        $prepare->execute();
        return $prepare;
    }

    

    

}
